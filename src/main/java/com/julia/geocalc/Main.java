package com.julia.geocalc;

import com.julia.geocalc.shapes.Point;
import com.julia.geocalc.shapes.Triangle;
import com.julia.geocalc.util.InputHelper;
import com.julia.geocalc.util.OutputHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void exit() {
        OutputHelper.printExit();
        System.exit(0);
    }

    public static Point coordinateReader(BufferedReader bufferedReader, String vertexName) throws IOException {
        Point point = null;
        while (point == null) {
            OutputHelper.askForCoordinate(vertexName);
            String coordinates = bufferedReader.readLine();
            if(InputHelper.isQuit(coordinates)) {
                Main.exit();
            }
            point = InputHelper.parseCoordinates(coordinates);
            if(point == null) {
                OutputHelper.printWrongCoordinates();
            }
        }
        return point;
    }

    public static void triangleReader(BufferedReader bufferedReader) throws IOException {
        OutputHelper.printGreetings();
        Point vertexA = Main.coordinateReader(bufferedReader, "A");
        Point vertexB = Main.coordinateReader(bufferedReader, "B");
        Point vertexC = Main.coordinateReader(bufferedReader, "C");
        Triangle triangle = new Triangle(vertexA, vertexB, vertexC);
        OutputHelper.printShapeArea(triangle.getArea());
        OutputHelper.printShapePerimeter(triangle.getPerimeter());
    }

    public static void main(String[] args) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                Main.triangleReader(bufferedReader);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
