package com.julia.geocalc.shapes;

/**
 * Created by Julia on 19.05.2017.
 */
public interface Shape {
    double getPerimeter();
    double getArea();
}
