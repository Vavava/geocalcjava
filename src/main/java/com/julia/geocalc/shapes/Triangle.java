package com.julia.geocalc.shapes;

import com.julia.geocalc.util.CoordinateHelper;

/**
 * Created by Julia on 19.05.2017.
 */
public class Triangle implements Shape {
    private Point vertexA;
    private Point vertexB;
    private Point vertexC;

    private double edgeAB;
    private double edgeBC;
    private double edgeAC;

    public Triangle(Point vertexA, Point vertexB, Point vertexC) {
        this.vertexA = vertexA;
        this.vertexB = vertexB;
        this.vertexC = vertexC;
        this.calculateEdges();
    }

    private void calculateEdges() {
        this.edgeAB = CoordinateHelper.calculateVectorModule(vertexA, vertexB);
        this.edgeBC = CoordinateHelper.calculateVectorModule(vertexB, vertexC);
        this.edgeAC = CoordinateHelper.calculateVectorModule(vertexC, vertexA);
    }

    public double getPerimeter() {
        return this.edgeAB + this.edgeBC + this.edgeAC;
    }

    public double getArea() {
        double halfPerimeter = this.getPerimeter()/2;
        return Math.sqrt(halfPerimeter
                *(halfPerimeter - this.edgeAB)
                *(halfPerimeter - this.edgeBC)
                *(halfPerimeter - this.edgeAC));
    }
}
