package com.julia.geocalc.shapes;

/**
 * Created by Julia on 30.05.2017.
 */
public class Vector {
    private double x;
    private double y;

    public Vector(Point from, Point to) {
        this.x = to.getX() - from.getX();
        this.y = to.getY() - from.getY();
    }

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
