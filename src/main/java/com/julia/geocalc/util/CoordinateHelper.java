package com.julia.geocalc.util;

import com.julia.geocalc.shapes.Point;
import com.julia.geocalc.shapes.Vector;

/**
 * Created by Julia on 19.05.2017.
 */
public class CoordinateHelper {
    public static double calculateVectorModule(Point firstPoint, Point secondPoint) {
        Vector vector = new Vector(firstPoint, secondPoint);
        return CoordinateHelper.calculateVectorModule(vector);
    }

    public static double calculateVectorModule(Vector vector) {
        return Math.sqrt(Math.pow(vector.getX(),2) + Math.pow(vector.getY(),2));
    }

    public static double calculateScalarMultiplication(Vector one, Vector two) {
        return one.getX()*two.getX() + one.getY()*two.getY();
    }

    public static double calculateAngle (Vector one, Vector two) {
        double firstModule = calculateVectorModule(one);
        double secondModule = calculateVectorModule(two);
        if(firstModule == 0 || secondModule == 0) {
            throw new ArithmeticException("Cannot calculate angle as one of the arguments is zero vector");
        }
        double scalarMultiplication = calculateScalarMultiplication(one, two);
        return Math.toDegrees(Math.acos(scalarMultiplication/firstModule/secondModule));
    }
}
