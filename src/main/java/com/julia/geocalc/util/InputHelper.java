package com.julia.geocalc.util;

import com.julia.geocalc.shapes.Point;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Julia on 20.05.2017.
 */
public class InputHelper {
    public static Point parseCoordinates(String str) {
        Pattern pattern = Pattern.compile("^(\\-?\\d+(\\.\\d+)?);\\s*(\\-?\\d+(\\.\\d+)?)$");
        Matcher matcher = pattern.matcher(str);
        if(matcher.matches()){
            double x = Double.parseDouble(matcher.group(1));
            double y = Double.parseDouble(matcher.group(3));
            return new Point(x,y);
        }
        return null;
    }

    public static boolean isQuit(String str) {
        return str.equals("q");
    }
}
