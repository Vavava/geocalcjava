package com.julia.geocalc.util;

/**
 * Created by Julia on 20.05.2017.
 */
public class OutputHelper {
    public static void askForCoordinate(String pointName) {
        System.out.println("Please type coordinates for vertex " + pointName);
    }

    public static void printShapeArea(double area) {
        System.out.println("Shape area is " + Double.toString(area));
    }

    public static void printShapePerimeter(double perimeter) {
        System.out.println("Shape perimeter is " + Double.toString(perimeter));
    }

    public static void printExit() {
        System.out.println("Exit!");
    }

    public static void printWrongCoordinates() {
        System.out.println("Wrong coordinates");
    }

    public static void printGreetings() {
        System.out.println("Let's calculate triangle parameters. " +
                "Type vertex coordinates in the format '12.75; -56.47'. Type 'q' to quit.");
    }

}
