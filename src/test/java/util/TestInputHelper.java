package util;

import com.julia.geocalc.shapes.Point;
import com.julia.geocalc.util.InputHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Julia on 20.05.2017.
 */
public class TestInputHelper extends Assert {
    @Test
    public void testParseCoordinates() {
        Point nullPoint = InputHelper.parseCoordinates("");
        Point stringPoint = InputHelper.parseCoordinates("string");
        Point commaDividerPoint = InputHelper.parseCoordinates("11,2; 11,5");
        Point noDecimalPartPoint = InputHelper.parseCoordinates("11.; 11.");

        Point positivePoint = InputHelper.parseCoordinates("1; 2");
        Point negativePoint = InputHelper.parseCoordinates("-1; -2");
        Point doublePoint = InputHelper.parseCoordinates("11.2; 11.5");

        assertNull(nullPoint);
        assertNull(commaDividerPoint);
        assertNull(noDecimalPartPoint);
        assertNull(stringPoint);

        assertEquals(new Point(1,2), positivePoint);
        assertEquals(new Point(-1,-2), negativePoint);
        assertEquals(new Point(11.2,11.5), doublePoint);
    }

    @Test
    public void testIsQuit() {
        assertTrue(InputHelper.isQuit("q"));
        assertFalse(InputHelper.isQuit("Q"));
        assertFalse(InputHelper.isQuit(""));
        assertFalse(InputHelper.isQuit("wrong"));
    }
}
